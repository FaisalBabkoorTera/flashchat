//
//  RegisterViewController.swift
//  Flash Chat iOS13
//
//  Created by Angela Yu on 21/10/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBAction func registerPressed(_ sender: UIButton) {
        guard let email = emailTextfield.text, !email.isEmpty,
              let pass = passwordTextfield.text, !pass.isEmpty else { return }
        Auth.auth().createUser(withEmail: email, password: pass) { result, error in
            if error != nil {
                debugPrint(error!.localizedDescription)
                return
            }else {
                self.performSegue(withIdentifier: SeguesID.RegisterToChat, sender: self)
            }
        }
    }
    
}
